import {BrowserModule} from '@angular/platform-browser';
import {NgModule, Provider} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomePageComponent} from './pages/home-page/home-page.component';
import {MainLayoutComponent} from './shared/main-layout/main-layout.component';
import {GameListPageComponent} from './pages/game-list-page/game-list-page.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {GameListComponent} from './shared/components/game-list/game-list.component';
import {GamePageComponent} from './pages/game-page/game-page.component';
import {LoginPageComponent} from './pages/login-page/login-page.component';
import {ReactiveFormsModule} from '@angular/forms';
import {PlayersListPageComponent} from './pages/players-list-page/players-list-page.component';
import {PlayerListComponent} from './shared/components/player-list/player-list.component';
import {PlayerPageComponent} from './pages/player-page/player-page.component';
import {NewGamePageComponent} from './pages/new-game-page/new-game-page.component';
import {NewPlayerPageComponent} from './pages/new-player-page/new-player-page.component';
import {AuthGuard} from './shared/services/auth.guard';
import {AuthService} from './shared/services/auth.service';
import {GameEditComponent} from './pages/game-page/game-edit/game-edit.component';
import {DpDatePickerModule} from 'ng2-date-picker';
import {OddsCalculatorPageComponent} from './pages/odds-calculator-page/odds-calculator-page.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {PlayerTableListComponent} from './pages/players-list-page/player-table-list/player-table-list.component';
import {PlayerChartComponent} from './pages/player-page/player-chart/player-chart.component';
import {CanAccessDirective} from './shared/services/can-access.directive';
import {BreadcrumbsComponent} from './shared/components/breadcrumbs/breadcrumbs.component';
import {AuthInterceptor} from './shared/auth.interceptor';
import {SpinnerService} from './shared/services/spinner/spinner.service';
import {LoaderInterceptor} from './shared/services/spinner/loader.interceptor';
import {SpinnerComponent} from './shared/components/spinner/spinner.component';
import {PlayerStatsByYearComponent} from './pages/player-page/player-stats-by-year/player-stats-by-year.component';

const INTERCEPTOR_AUTH: Provider = {
  provide: HTTP_INTERCEPTORS,
  multi: true,
  useClass: AuthInterceptor
};

const INTERCEPTOR_LOADER: Provider = {
  provide: HTTP_INTERCEPTORS,
  multi: true,
  useClass: LoaderInterceptor
};

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    MainLayoutComponent,
    GameListPageComponent,
    GameListComponent,
    GamePageComponent,
    LoginPageComponent,
    PlayersListPageComponent,
    PlayerListComponent,
    PlayerPageComponent,
    NewGamePageComponent,
    NewPlayerPageComponent,
    GameEditComponent,
    OddsCalculatorPageComponent,
    PlayerTableListComponent,
    PlayerChartComponent,
    CanAccessDirective,
    BreadcrumbsComponent,
    SpinnerComponent,
    PlayerStatsByYearComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    DpDatePickerModule,
    NgSelectModule,
  ],
  providers: [
    AuthGuard,
    AuthService, INTERCEPTOR_AUTH,
    SpinnerService, INTERCEPTOR_LOADER,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
