import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../shared/services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required)
  });

  constructor(
    private fb: FormBuilder,
    public authService: AuthService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    if (this.authService.isAuthenticated()) {
      this.navigateToHome();
    }
  }

  handleSubmit(): void {
    if (this.form.invalid) {
      return;
    }

    this.authService.login(this.form.value)
      .subscribe(res => {
        this.navigateToHome();
      });
  }

  private navigateToHome(): void {
    this.router.navigate(['/']);
  }
}
