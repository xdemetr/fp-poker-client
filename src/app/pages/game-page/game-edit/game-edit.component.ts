import {Component, OnInit} from '@angular/core';
import {GameService} from '../../../shared/services/game.service';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {Game} from '../../../shared/services/Game';
import {FormBuilder, Validators} from '@angular/forms';
import {GameEditForm, initialGameEditForm, transformGameEditForm} from './game-edit-form';
import {RouteUrl} from '../../../route-url';

@Component({
  selector: 'app-game-edit',
  templateUrl: './game-edit.component.html',
  styleUrls: ['./game-edit.component.scss']
})
export class GameEditComponent implements OnInit {

  routeSub: Subscription;
  routeUrl = RouteUrl;
  game: Game;
  // @ts-ignore
  form = this.fb.group<GameEditForm>(initialGameEditForm);
  config: any;

  constructor(
    private gameService: GameService,
    private route: ActivatedRoute,
    private router: Router,
    private fb: FormBuilder
  ) {
  }

  ngOnInit(): void {
    this.routeSub = this.route.params.subscribe(params => {
      if (params.id) {
        this.gameService.get(params.id).subscribe(
          game => {
            this.game = game;
            const results = this.fb.array(game.players.map((pl, idx) => {
              return this.fb.control(this.game.results[idx], Validators.required);
            }));
            this.form.setControl('results', results);

            this.form.patchValue({name: this.game.name});
          }
        );
      }
    });
  }

  handleSubmit(): void {
    this.gameService.saveResult(transformGameEditForm(this.form.value, this.game.players, this.game._id))
      .subscribe(r => {
        this.router.navigate([this.routeUrl.GAMES, r.name]);
      });
  }

  setResult(idx: number, value: number): void {
    (this.form.controls.results as any).controls[idx].setValue(value);
  }
}
