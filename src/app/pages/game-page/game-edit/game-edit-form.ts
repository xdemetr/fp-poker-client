import {Player} from '../../../shared/services/Player';

export class GameEditForm {
  results: number[];
  name: Date;
}

export const initialGameEditForm: GameEditForm = {
  results: null,
  name: null
};

export const transformGameEditForm = (form: GameEditForm, players: Player[], id) => {
  return {
    id,
    name: form.name,
    players: players.map(p => p._id),
    results: form.results
  };
};
