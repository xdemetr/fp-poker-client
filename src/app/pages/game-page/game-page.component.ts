import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {GameService} from '../../shared/services/game.service';
import {Game} from '../../shared/services/Game';
import {RouteUrl} from '../../route-url';

@Component({
  selector: 'app-game-page',
  templateUrl: './game-page.component.html',
  styleUrls: ['./game-page.component.scss']
})
export class GamePageComponent implements OnInit {

  routeUrl = RouteUrl;
  game: Game;

  constructor(
    private route: ActivatedRoute,
    private gameService: GameService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      if (params.name) {
        this.gameService.get(params.name).subscribe(
          game => this.game = game
        );
      }
    });
  }

  delete(): void {
    if (confirm(`Удалить игру ${this.game.name}?`)) {
      this.gameService.delete(this.game._id)
        .subscribe(() => {
          this.router.navigate(['/']);
        });
    }
  }
}
