import {Component, OnInit} from '@angular/core';
import {PlayerService} from '../../../shared/services/player.service';
import {Player} from '../../../shared/services/Player';
import {RouteUrl} from '../../../route-url';

@Component({
  selector: 'app-player-table-list',
  templateUrl: './player-table-list.component.html',
  styleUrls: ['./player-table-list.component.scss']
})
export class PlayerTableListComponent implements OnInit {

  allPlayers: Player[];
  players: Player[];
  hiddenPlayers: Player[];
  routeUrl = RouteUrl;
  isShowHidden: boolean;

  constructor(
    private playerService: PlayerService
  ) {
    this.isShowHidden = false;
  }

  ngOnInit(): void {
    this.playerService.getAll('balance')
      .subscribe(players => {
        this.allPlayers = players;
        this.players = players.filter(pl => pl.isShowInRating);
        this.hiddenPlayers = players.filter(pl => !pl.isShowInRating);
      });
  }

  showHiddenPlayers(): void {
    this.isShowHidden = !this.isShowHidden;

    if (this.isShowHidden) {
      this.players = [...this.allPlayers];
    } else {
      this.players = this.allPlayers.filter(pl => pl.isShowInRating);
    }
  }
}
