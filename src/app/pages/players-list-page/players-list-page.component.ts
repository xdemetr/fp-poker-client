import {Component, OnInit} from '@angular/core';
import {RouteUrl} from '../../route-url';

@Component({
  selector: 'app-players-list-page',
  templateUrl: './players-list-page.component.html',
  styleUrls: ['./players-list-page.component.scss']
})
export class PlayersListPageComponent implements OnInit {

  routeUrl = RouteUrl;


  constructor() {
  }

  ngOnInit(): void {

  }

}
