import {Component, OnInit} from '@angular/core';
import {PlayerService} from '../../shared/services/player.service';
import {Player} from '../../shared/services/Player';
import {FormBuilder} from '@angular/forms';
import {initialNewGameForm, NewGameForm, transformNewGameForm} from './new-game-form';
import {GameService} from '../../shared/services/game.service';
import {Router} from '@angular/router';
import {IDatePickerDirectiveConfig} from 'ng2-date-picker/date-picker/date-picker-directive-config.model';
import {RouteUrl} from '../../route-url';

@Component({
  selector: 'app-new-game-page',
  templateUrl: './new-game-page.component.html',
  styleUrls: ['./new-game-page.component.scss']
})
export class NewGamePageComponent implements OnInit {

  players: Player[];
  // @ts-ignore
  form = this.fb.group<NewGameForm>(initialNewGameForm);
  config: IDatePickerDirectiveConfig = {};
  routeUrl = RouteUrl;

  checkedPlayers: number;

  constructor(
    private playerService: PlayerService,
    private gameService: GameService,
    private fb: FormBuilder,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.playerService.getAll().subscribe(res => {
      this.players = res;
      const groups = this.fb.array(res.map(
        group => this.fb.group({
            _id: group._id,
            isChecked: false
          }
        )
      ));

      this.form.setControl('players', groups);
    });

    this.form.valueChanges.subscribe(value => {
      this.checkedPlayers = transformNewGameForm(value).players.length;
    });

  }

  handleSubmit(): void {
    this.gameService.saveNew(transformNewGameForm(this.form.value))
      .subscribe(res => {
        this.router.navigate([this.routeUrl.GAMES, res.name]);
      });
  }
}
