import {Player} from '../../shared/services/Player';

export class NewGameForm {
  name: string;
  players: Player[];
  buyIn: number;
  isBigGame: boolean;
}

export const initialNewGameForm: NewGameForm = {
  name: null,
  players: [],
  buyIn: 500,
  isBigGame: false,
};

export const transformNewGameForm = (form) => {
  const players = form.players
    .filter(pl => pl.isChecked)
    .map(pl => pl._id);

  return {
    ...form,
    players
  };
};
