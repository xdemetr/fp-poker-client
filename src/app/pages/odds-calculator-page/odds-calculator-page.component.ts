import {Component, OnInit} from '@angular/core';
import {CardGroup, OddsCalculator} from 'poker-odds-calculator';
import {FormBuilder, Validators} from '@angular/forms';
import {initialOddsCalcForm, OddsCalcForm, transformOddsCalcForm} from './odds-calc-form';

@Component({
  selector: 'app-odds-calculator-page',
  templateUrl: './odds-calculator-page.component.html',
  styleUrls: ['./odds-calculator-page.component.scss']
})
export class OddsCalculatorPageComponent implements OnInit {

  result: OddsCalculator;

  emptyArray: any[];
  playerCards: any[];
  suitImg = ['♠', '♣', '♥', '♦'];
  suitId = ['s', 'c', 'h', 'd'];
  rank = ['A', 'K', 'Q', 'J', 'T', '9', '8', '7', '6', '5', '4', '3', '2'];

  startCards: any = this.suitId.map((s, suitIdx) => {
    return this.rank.map((r, idx) => {
      return {
        id: r + s,
        label: (r === 'T' ? '10' : r) + this.suitImg[suitIdx]
      };
    });
  });

  availableCards = [];

  playersForSelect = [2, 3, 4, 5, 6];

  // @ts-ignore
  form = this.fb.group<OddsCalcForm>(initialOddsCalcForm);

  constructor(
    private fb: FormBuilder
  ) {
  }

  ngOnInit(): void {
    this.startCards = [...this.startCards[0], ...this.startCards[1], ...this.startCards[2], ...this.startCards[3]];
    this.availableCards = [...this.startCards];


    this.form.controls.playerCount.valueChanges.subscribe(values => {
      this.setPlayerCount(values);
    });

    this.setPlayerCount(this.form.controls.playerCount.value);
    this.setBoard();
  }

  private setPlayerCount(values): void {
    const emptyArray = Array.apply(null, Array(+values)).map((x, i) => i);
    const players = this.fb.array(emptyArray.map(() => {
      return this.fb.array([
        this.fb.control('', Validators.required),
        this.fb.control('', Validators.required)
      ]);
    }));
    this.form.setControl('playerCards', players);
  }

  private setBoard(): void {
    const emptyBoard = Array.apply(null, Array(5)).map((x, i) => i);
    const board = this.fb.array(emptyBoard.map(() => {
      return this.fb.control('');
    }));
    this.form.setControl('board', board);
  }

  renderCards(cards: any): any {
    return cards.map(c => {
      if (c) {
        return c.toString();
      }
    });
  }

  isRedSuit(card: string): boolean {
    if (!card) {
      return;
    }
    return (card[1] === 'd' || card[1] === 'h');
  }

  getEquity(index): string {
    if (!this.result) {
      return;
    }
    return +this.result.equities[index].getEquity().toString() + '%';
  }

  getHandrank(index): string {
    if (!this.result) {
      return;
    }
    return this.result.getHandRank(index).toString();
  }

  getCardControls(): any {
    return (this.form.controls.playerCards as any).controls;
  }

  handleSubmit(): void {

    const cards = transformOddsCalcForm(this.form.value).playerCards.map(pl => CardGroup.fromString(pl));
    const board = CardGroup.fromString(transformOddsCalcForm(this.form.value).boardCards);

    this.result = OddsCalculator.calculate(cards, board);
  }

  getPlayerCardsControls(index: number): any {
    return ((this.form.controls.playerCards as any).controls[index]).controls;
  }

  getBoardCardsControls(): any {
    return (this.form.controls.board as any).controls;
  }

  getBoardCardsControlsValue(): any {
    return (this.form.controls.board as any).value;
  }

  getPlayerCardsControlsValue(index: number): any {
    return ((this.form.controls.playerCards as any).controls[index]).value;
  }

  onCardSelect(e: any): void {
    const cards = this.availableCards.filter(c => c !== e);
    this.availableCards = [...cards];
  }

  reset(event): void {
    event.preventDefault();

    this.availableCards = [...this.startCards];
    this.form.reset();
    this.result = null;
    this.setPlayerCount(this.form.controls.playerCount.value);
    this.setBoard();
  }
}
