export class OddsCalcForm {
  playerCount: number;
  playerCards: string[][];
  board: string[];
}

export const initialOddsCalcForm: OddsCalcForm = {
  playerCount: 2,
  playerCards: [],
  board: [],
};

export const transformOddsCalcForm = (form: OddsCalcForm) => {
  const playerCards = form.playerCards.map(pl => {
    return pl.join('');
  });

  const boardCards = form.board.join('');

  return {
    playerCards,
    boardCards
  };
};
