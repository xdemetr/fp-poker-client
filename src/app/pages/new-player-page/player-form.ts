import {BaseItem} from '../../shared/services/BaseItem';

export class PlayerForm extends BaseItem {
  handle: string;
  isRegular: boolean;
  isShowInRating: boolean;
}

export const initialPlayerForm: PlayerForm = {
  _id: null,
  name: null,
  handle: null,
  isRegular: null,
  isShowInRating: null,
};
