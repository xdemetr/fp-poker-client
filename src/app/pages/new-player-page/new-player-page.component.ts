import {Component, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {initialPlayerForm, PlayerForm} from './player-form';
import {PlayerService} from '../../shared/services/player.service';
import {ActivatedRoute, Router} from '@angular/router';
import {RouteUrl} from '../../route-url';
import {Subscription} from 'rxjs';
import {Player} from '../../shared/services/Player';

@Component({
  selector: 'app-new-player-page',
  templateUrl: './new-player-page.component.html',
  styleUrls: ['./new-player-page.component.scss']
})
export class NewPlayerPageComponent implements OnInit {
  routeSub: Subscription;
  routeUrl = RouteUrl;
  player: Player;

  constructor(
    private fb: FormBuilder,
    private playerService: PlayerService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
  }

  // @ts-ignore
  form = this.fb.group<PlayerForm>(initialPlayerForm);

  ngOnInit(): void {
    this.form.controls.name.setValidators(Validators.required);
    this.form.controls.handle.setValidators(Validators.required);

    this.routeSub = this.route.params.subscribe(params => {

      if (params.id) {
        this.playerService.get(params.id).subscribe(res => {
          this.form.patchValue(res);
          this.player = res;
        });
      }
    });
  }

  handleSubmit(): void {
    this.playerService.save(this.form.value).subscribe(res => {
      this.router.navigate([this.routeUrl.PLAYERS, res.handle]);
    });
  }

  getHeader(): string {
    return this.player ? 'Редактирование игрока' : 'Добавление игрока';
  }

  cancel(): void {
    this.router.navigate([
      this.routeUrl.PLAYERS,
      this.player ? this.player.handle : '']
    );
  }
}
