import {Component, OnInit} from '@angular/core';
import {forkJoin, Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {PlayerService} from '../../shared/services/player.service';
import {Player} from '../../shared/services/Player';
import {RouteUrl} from '../../route-url';

import * as d3 from 'd3';
import {GameService} from '../../shared/services/game.service';

@Component({
  selector: 'app-player-page',
  templateUrl: './player-page.component.html',
  styleUrls: ['./player-page.component.scss']
})
export class PlayerPageComponent implements OnInit {

  routeSub: Subscription;
  player: Player;
  routeUrl = RouteUrl;
  gamesCount: number;
  average: number;
  handle: string;

  constructor(
    private route: ActivatedRoute,
    private playerService: PlayerService,
    private gameService: GameService,
  ) {
  }


  ngOnInit(): void {

    this.routeSub = this.route.params.subscribe(params => {
      if (params.handle) {
        this.handle = params.handle;

        forkJoin([
          this.playerService.getByHandle(this.handle),
          this.gameService.getCount()
        ]).subscribe(([player, gamesCount]) => {

          this.player = player;
          this.gamesCount = gamesCount;

          this.average = Math.round(this.player.balance / this.player.results.length);
        });
      }
    });
  }

}
