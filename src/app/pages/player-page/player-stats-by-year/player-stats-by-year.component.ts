import {Component, Input, OnInit} from '@angular/core';
import {GameResult} from '../../../shared/services/Player';
import {RouteUrl} from '../../../route-url';


interface Group {
  year: string;
  games: GameResult[];
  result?: number;
  isOpen: boolean;
}

@Component({
  selector: 'app-player-stats-by-year',
  templateUrl: './player-stats-by-year.component.html',
  styleUrls: ['./player-stats-by-year.component.scss']
})

export class PlayerStatsByYearComponent implements OnInit {
  // tslint:disable-next-line:variable-name
  _games: GameResult[];

  get games(): GameResult[] {
    return this._games;
  }

  @Input() set games(gamesResult) {
    this._games = gamesResult;
    this.update();
  }

  stats: Group[];
  routeUrl = RouteUrl;

  constructor() {
  }

  ngOnInit(): void {
    this.update();
  }

  update(): void {
    const groupsArray = this._games.reduce((group, result) => {
      const date = result.game.date.toString().split('-')[0];

      if (!group[date]) {
        group[date] = [];
      }
      group[date].push({
        ...result.game,
        result: result.result,
      });
      return group;
    }, {});

    const groupArrays: Group[] = Object.keys(groupsArray).map((year) => {
      const resByYear = groupsArray[year].reduce((acc, item) => {
        return acc + item.result;
      }, 0);


      return {
        year,
        games: groupsArray[year],
        result: resByYear,
        isOpen: false
      };
    }).sort((a, b) => (a.year < b.year ? 1 : -1));


    this.stats = groupArrays;

  }

  toggle(i: number): void {
    this.stats[i].isOpen = !this.stats[i].isOpen;
  }
}
