import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MainLayoutComponent} from './shared/main-layout/main-layout.component';
import {HomePageComponent} from './pages/home-page/home-page.component';
import {GameListPageComponent} from './pages/game-list-page/game-list-page.component';
import {GamePageComponent} from './pages/game-page/game-page.component';
import {LoginPageComponent} from './pages/login-page/login-page.component';
import {PlayersListPageComponent} from './pages/players-list-page/players-list-page.component';
import {PlayerPageComponent} from './pages/player-page/player-page.component';
import {NewGamePageComponent} from './pages/new-game-page/new-game-page.component';
import {NewPlayerPageComponent} from './pages/new-player-page/new-player-page.component';
import {AuthGuard} from './shared/services/auth.guard';
import {GameEditComponent} from './pages/game-page/game-edit/game-edit.component';
import {OddsCalculatorPageComponent} from './pages/odds-calculator-page/odds-calculator-page.component';
import {PlayerTableListComponent} from './pages/players-list-page/player-table-list/player-table-list.component';

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    data: {
      breadcrumb: 'Главная'
    },
    children: [
      {
        path: '',
        component: HomePageComponent,
      },
      {
        path: 'games',
        component: GameListPageComponent,
        data: {
          breadcrumb: 'Игры'
        },
        children: [
          {
            path: 'new',
            component: NewGamePageComponent,
            canActivate: [AuthGuard],
            data: {
              breadcrumb: 'Новая',
            },
          },
          {
            path: 'edit/:id',
            component: GameEditComponent,
            canActivate: [AuthGuard],
            data: {
              breadcrumb: 'Редактирование',
            },
          },
          {
            path: ':name',
            component: GamePageComponent,
            data: {
              breadcrumb: '',
            },
          },
        ]
      },
      {
        path: 'players',
        component: PlayersListPageComponent,
        data: {
          breadcrumb: 'Игроки'
        },
        children: [
          {
            path: '',
            component: PlayerTableListComponent,
          },
          {
            path: 'new',
            component: NewPlayerPageComponent,
            canActivate: [AuthGuard],
          },
          {
            path: ':handle',
            component: PlayerPageComponent,
            data: {
              breadcrumb: '',
            },
          },
          {
            path: 'edit/:id',
            component: NewPlayerPageComponent,
            canActivate: [AuthGuard],
          },
        ]
      },
      {
        path: 'login',
        component: LoginPageComponent,
        data: {
          breadcrumb: 'Войти'
        },
      },
      {
        path: 'logout',
        component: HomePageComponent
      },
      {
        path: 'odds-calc',
        component: OddsCalculatorPageComponent,
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {scrollPositionRestoration: 'enabled'})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
