export enum RouteUrl {
  GAMES = '/games',
  PLAYERS = '/players',
  NEW_PLAYER = '/new-player',
  GAME_EDIT = '/games/edit',
  LOGIN = '/login',
}
