import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {AuthService} from '../services/auth.service';
import {RouteUrl} from '../../route-url';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnInit {

  routeUrl = RouteUrl;
  isNavBarShow = false;

  constructor(
    public authService: AuthService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    // @ts-ignore
    this.router.events.subscribe((event: Event) => {
      if (event instanceof NavigationEnd) {
        this.isNavBarShow = false;
      }
    });
  }

  showNavBar(): void {
    this.isNavBarShow = !this.isNavBarShow;
  }
}
