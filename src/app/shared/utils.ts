import {assign, isArray, isDate, isEmpty, isNil, isString, mapValues, omitBy} from 'lodash';
import {format, isValid} from 'date-fns';
import {Params} from '@angular/router';


export const toQueryParams = (object: any): Params => {
  const filtered = omitBy(object, (val) =>
    isNil(val)
    || (isString(val) || isArray(val)) && isEmpty(val)
    || (isDate(val) && !isValid(val)));

  // return mapValues(filtered, (val) => isDate(val) ? stringifyDate(val) : val.toString());
  return mapValues(filtered);
};
