import {BaseItem} from './BaseItem';
import {Game} from './Game';

export class Player extends BaseItem {
  handle: string;
  results: GameResult[];
  balance: number;
  gameCount: number;
  maxSeriesOfWin: number;
  maxSeriesOfLoose: number;
  isShowInRating: boolean;
  isRegular: boolean;
}

export class GameResult extends BaseItem {
  game: Game;
  result: number;
  isBigGame: boolean;
}
