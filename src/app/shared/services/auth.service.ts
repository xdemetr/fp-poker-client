import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable, Subject, throwError} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {User} from './User';

interface Token {
  success: boolean;
  token: string;
  exp: number;
}

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  baseUrl: string;
  TOKEN_KEY = 'fb-token';
  TOKEN_EXP_DATE = 'fb-token-exp';
  public error$: Subject<string> = new Subject<string>();

  constructor(
    private http: HttpClient,
  ) {
    this.baseUrl = `${environment.apiUrl}/users`;
  }

  get token(): string {
    const expDate = new Date(localStorage.getItem(this.TOKEN_EXP_DATE));
    if (expDate && new Date() > expDate) {
      this.logout();
      return null;
    }
    return localStorage.getItem(this.TOKEN_KEY);
  }

  login(user: User): Observable<any> {
    return this.http.post(`${this.baseUrl}/login`, user)
      .pipe(
        tap(res => this.setToken(res as Token)),
        catchError(this.handleError.bind(this))
      );
  }

  logout(): void {
    this.setToken(null);
  }

  isAuthenticated(): boolean {
    return !!this.token;
  }

  private handleError(error: HttpErrorResponse): Observable<string> {
    this.error$.next(error.error.message);
    return throwError(error.error.message);
  }

  private setToken(res: Token | null): void {
    if (res?.success) {
      const expDate = new Date(new Date().getTime() + +res.exp * 1000);
      localStorage.setItem(this.TOKEN_KEY, res.token);
      localStorage.setItem(this.TOKEN_EXP_DATE, expDate.toString());
    } else {
      localStorage.removeItem(this.TOKEN_KEY);
      localStorage.removeItem(this.TOKEN_EXP_DATE);
    }
  }
}
