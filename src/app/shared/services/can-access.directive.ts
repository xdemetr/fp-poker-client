import {Directive, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {AuthService} from './auth.service';

@Directive({
  selector: '[appCanAccess]'
})
export class CanAccessDirective implements OnInit {
  constructor(private templateRef: TemplateRef<any>,
              private viewContainer: ViewContainerRef,
              private authService: AuthService,
  ) {
  }

  ngOnInit(): void {
    this.applyPermission();
  }


  private applyPermission(): void {
    if (this.authService.isAuthenticated()) {
      this.viewContainer.createEmbeddedView(this.templateRef);
    } else {
      this.viewContainer.clear();
    }
  }
}
