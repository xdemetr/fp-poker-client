import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {plainToClass} from 'class-transformer';
import {Player} from './Player';
import {toQueryParams} from '../utils';

@Injectable({
  providedIn: 'root'
})
export class PlayerService {
  baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = `${environment.apiUrl}/players`;
  }

  getAll(sortBy?: string): Observable<Player[]> {
    return this.http.get<Player[]>(`${this.baseUrl}`, {params: toQueryParams({sortBy})})
      .pipe(map((res) => plainToClass(Player, res)));
  }

  get(id: string): Observable<Player> {
    return this.http.get(`${this.baseUrl}/${id}`)
      .pipe(map((res) => plainToClass(Player, res)));
  }

  getByHandle(handle: string): Observable<Player> {
    return this.http.get(`${this.baseUrl}/handle/${handle}`)
      .pipe(map((res) => plainToClass(Player, res)));
  }

  save(player): Observable<any> {
    return this.http.post(`${this.baseUrl}`, player)
      .pipe(map((res) => plainToClass(Player, res)));
  }
}
