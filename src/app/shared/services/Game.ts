import {BaseItem} from './BaseItem';
import {Player} from './Player';

export class Game extends BaseItem {
  date: Date;
  results: number[];
  players: Player[];
  isBigGame: boolean;
  buyIn: number;
}
