import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Game} from './Game';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {plainToClass} from 'class-transformer';
import {PageResponse} from '../pagination/page-response';
import {toQueryParams} from '../utils';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  baseUrl: string;


  constructor(private http: HttpClient) {
    this.baseUrl = `${environment.apiUrl}/games`;
  }

  getAll(page?: number, limit?: number): Observable<PageResponse<Game>> {
    return this.http.get<PageResponse<any>>(`${this.baseUrl}`, {
      params: toQueryParams({
        page,
        limit
      })
    })
      .pipe(map(res => ({
        data: plainToClass(Game, res.data),
        pages: res.pages
      })));
  }

  getCount(): Observable<any> {
    return this.http.get(`${this.baseUrl}/count`);
  }

  get(id: string): Observable<Game> {
    return this.http.get(`${this.baseUrl}/${id}`)
      .pipe(map((res) => plainToClass(Game, res)));
  }

  saveNew(body): Observable<any> {
    return this.http.post(`${this.baseUrl}/new`, body)
      .pipe(map((res) => plainToClass(Game, res)));
  }

  saveResult(body): Observable<any> {
    return this.http.post(`${this.baseUrl}/save-result`, body)
      .pipe(map((res) => plainToClass(Game, res)));

  }

  delete(id: string): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`)
      .pipe(map((res) => plainToClass(Game, res)));
  }
}
