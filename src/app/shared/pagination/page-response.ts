export class PageResponse<T> {
  constructor(
    public data: T[],
    public pages: PaginationItem[]) {
  }
}

export interface PaginationItem {
  number: number;
  url: string;
}
