import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {AuthService} from './services/auth.service';
import {Router} from '@angular/router';
import {catchError} from 'rxjs/operators';
import {RouteUrl} from '../route-url';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthService,
    private router: Router,
  ) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    if (this.authService.isAuthenticated() && request.method !== 'GET') {
      request = request.clone({
        setHeaders: {
          Authorization: this.authService.token
        }
      });
    }

    return next.handle(request)
      .pipe(
        catchError((e: HttpErrorResponse) => {
          if (e.status === 401) {
            this.authService.logout();
            this.router.navigate([RouteUrl.LOGIN]);
          }

          return throwError(e);
        })
      );
  }
}
