import {Component, OnInit} from '@angular/core';
import {PlayerService} from '../../services/player.service';
import {Player} from '../../services/Player';
import {RouteUrl} from '../../../route-url';

@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.scss']
})
export class PlayerListComponent implements OnInit {

  routeUrl = RouteUrl;
  players: Player[];

  constructor(
    private playerService: PlayerService
  ) {
  }

  ngOnInit(): void {
    this.playerService.getAll().subscribe(res => this.players = res);
  }

}
