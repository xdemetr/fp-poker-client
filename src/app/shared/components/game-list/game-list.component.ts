import {Component, OnInit} from '@angular/core';
import {Game} from '../../services/Game';
import {GameService} from '../../services/game.service';
import {RouteUrl} from '../../../route-url';
import {PaginationItem} from '../../pagination/page-response';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.scss']
})
export class GameListComponent implements OnInit {
  routeUrl = RouteUrl;
  games: Game[];

  pages: PaginationItem[];
  currentPage: number;

  constructor(
    private gameService: GameService,
  ) {
    this.currentPage = 1;
  }

  ngOnInit(): void {
    this.refresh();
  }

  refresh(): void {
    this.gameService.getAll(this.currentPage)
      .subscribe((games) => {
        this.games = games.data;
        this.pages = games.pages;
      });
  }

  handlePagerClick(page: any): void {
    this.currentPage = page;
    this.refresh();
  }
}
